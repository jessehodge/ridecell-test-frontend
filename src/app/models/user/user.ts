export class User {
  username: string;
  email: string;
  password: string;
  user_type: string;
  name: string;
  lon: string;
  lat: string;
  token: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
