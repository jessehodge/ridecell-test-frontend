export class ParkingSpot {
  public id: number;
  public latitude: string;
  public longitude: string;
  public radius: number;
  public reserved: boolean;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
