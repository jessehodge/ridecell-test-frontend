import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {ParkingSpot} from '../models/parking_spots/parking-spots';
import {User} from '../models/user/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private isAuthenticated: boolean;
  public parking_spots = [];
  public parking_spot = new ParkingSpot();
  public user = new User();
  public nav: boolean;
  displayedColumns: string[] = ['latitude', 'longitude', 'reserved', 'reserve', 'remove'];
  private base_url = 'http://206.189.230.238/api/v1/';
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('token') != null) {
      this.isAuthenticated = true;
    } else {
      this.router.navigateByUrl('');
    }
    if (navigator.geolocation) {
      this.nav = true;
      navigator.geolocation.getCurrentPosition(position => {
        this.user.lat = position.coords.latitude.toString();
        this.user.lon = position.coords.longitude.toString();
      });
    }
    this.http.get<any>('http://206.189.230.238/api/v1/parking_spot/').subscribe(
      data => {
        this.parking_spots = data;
      }, error => {console.log(error); });
  }

  public search(parking_spot) {
    const url = this.base_url + 'search/' + parking_spot.latitude + '/' + parking_spot.longitude + '/' + parking_spot.radius + '/';
    this.http.get<any>(url).subscribe(
      data => {
        console.log(data);
        this.parking_spots = data;
      });
  }

  public reserve(element) {
    element.reserved = true;
    this.http.patch<ParkingSpot>(this.base_url + 'parking_spot/' + element.id + '/', element).subscribe(
      data => {
        console.log(data);
      });
  }

  public remove_reservation(element) {
    element.reserved = false;
    this.http.patch<ParkingSpot>(this.base_url + 'parking_spot/' + element.id + '/', element).subscribe(
      data => {
        console.log(data);
      });
  }

}
