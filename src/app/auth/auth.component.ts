import { Component, OnInit } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {User} from '../models/user/user';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  public user = new User();

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {

  }

  register(user) {
    user.user_type = 'parker';
    this.http.post<User>('http://206.189.230.238/api/v1/auth/register/', user)
      .subscribe(data => {console.log(data); }, error => {console.log(error); });
  }

  login(user) {
    this.http.post<User>('http://206.189.230.238/api/v1/auth/login/', user)
      .subscribe(data => {
        localStorage.setItem('token', data.token);
      }, error => {
        console.log(error)
      }, () => this.router.navigate(['/dashboard']));
  }

}
